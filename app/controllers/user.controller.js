const db = require("../models");
const Todo = db.todo;

exports.allAccess = (req, res) => {
  res.status(200).send("Todo List.");
};

exports.userBoard = (req, res) => {
  res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

exports.todoCreate = (req,res) =>{
  const todo = new Todo({
    title: req.body.title,
    user: req.userId,
  })
  todo
    .save()
    .then(result => {
      res.status(201).json({
        message: 'Todo added',
        createdTodo: {
          title: result.title,
          done: result.done,
          date: result.date,
        }
      })
    })
    .catch(err => {
      res.status(500).json({
        error: err
      })
    })
}

exports.todoGetAll = (req, res) => {
  Todo.find({
    user: req.userId,
  })
  .exec()
  .then(result => {
    res.status(200).json(result)
  })
  .catch(err =>{
    res.status(500).json({
      error: err
    })
  })
};

exports.todoDelete = (req,res) =>{
  const id = req.params.todoId
  Todo.deleteOne({ _id: id })
    .exec()
    .then(() => {
      res.status(200).json({
        message: 'Todo deleted successfully'
      })
    })
    .catch(err => {
      res.status(500).json({
        error: err
      })
    })
}

exports.todoCompleted = (req,res) => {
  const id = req.params.todoId
  Todo.findById(id)
    .exec()
    .then(result => {
      console.log(result);
      result.done = !result.done
      return result.save()
    })
    .then(result => {
      res.status(200).json({
        message: 'Done status updated',
        result
      })
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      })
    })
}