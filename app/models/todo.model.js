const mongoose = require("mongoose")

const Todo = mongoose.model(
    "Todo",
    new mongoose.Schema({
        title: {
            type: String,
            required: true
            },
        done: {
            type: Boolean,
            default: false
                },
        date: {
            type: Date,
            default: Date.now()
        },
        user:
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: "User"
            },
    })
)

module.exports = Todo;